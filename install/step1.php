<form action="<?echo $APPLICATION->GetCurPage()?>" name="form1">
<?=bitrix_sessid_post()?>
<?
	if(($_REQUEST["install"]=="Y")&&($_REQUEST["step"]==2)&&(!sizeof($_REQUEST["LID"])>0)) 
	{
		CAdminMessage::ShowMessage("Пожалуйста, выберите хотя бы один сайт");	
	}	

	// get list of sites
	$arSites = CSite::GetList($by="id", $order="asc", Array());
	// if any site found
	if($arSites->SelectedRowsCount() > 0) 
	{	
		$idSites = array();
?>
		<table class="adm-detail-content-table edit-table">
			<tr>
				<td><b>Выберите сайт(ы) для установки почтового шаблона:</b></td>
			</tr>
			<td><br></td>
<?
			while ($arSite = $arSites->Fetch())
			{
				$idSites[] = $arSite["LID"];
			}
?>
			<tr class="adm-detail-required-field">
				<td><?=CLang::SelectBoxMulti("LID", $str_LID);?></td>
			</tr>
		</table>
		<br>
		<input type="hidden" name="install" value="Y">
		<input type="submit" name="inst" value="Установить">
		<input type="hidden" name="step" value="2">		
<?
	}
	else
	{
		CAdminMessage::ShowMessage("Ошибка получения списка сайтов.<br>Продолжение установки не возможно.");	
?>
		<input type="hidden" name="install" value="N">
		<input type="submit" name="" value="Закрыть">
		<input type="hidden" name="step" value="1">
<?
	}
?>
		<input type="hidden" name="id" value="pwdreset">
</form>
