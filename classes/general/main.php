<?

use \Bitrix\Main\Security\Random;

class CPwdreset
{

	function getSiteName($sid)
	{
		// get default site name
		$rsSites = CSite::GetByID($siteID);
		$arSite = $rsSites->Fetch();
		$siteName = ($arSite && $arSite['NAME']) ? $siteName = $arSite['NAME'] : $siteID;
		return $siteName;
	}
	
	
	function randomPassword($gid) 
	{
		
		if(!is_array($gid) && is_numeric($gid) && $gid > 0) 
		{
			$gid = array($gid);
		}
		
		$policy = CUser::GetGroupPolicy($gid);

		$length = $policy['PASSWORD_LENGTH'];
		if ($length <= 0) 
		{
			$length = 6;
		}
		
		$alphabet = Random::ALPHABET_ALPHAUPPER 
					| Random::ALPHABET_ALPHALOWER
					| Random::ALPHABET_NUM;
		
		if ($policy['PASSWORD_PUNCTUATION'] == 'Y') 
		{
			$alphabet |= Random::ALPHABET_SPECIAL;
		}
			
		$password = Random::getStringByAlphabet($length, $alphabet);
			
		if ($policy['PASSWORD_DIGITS'] == 'Y') 
		{
			if (!preg_match('/[0-9]+/', $password)) {
				$password .= (string) rand(0, 9);
			}			
		}

		return $password;

	}
	
	
	function newPassword($uid,$login,$email,$gid,$siteID,$siteName)
	{
		
		$user = new CUser;
		$password = CPwdreset::randomPassword($gid);
		
		if($password)
		{
		
			if ($user->Update($uid, Array("PASSWORD"=>$password,"CONFIRM_PASSWORD"=>$password)))
			{

				$password=$password.'';
			
				$arFields = array(
					"SITE_NAME" => $siteName,
					"LOGIN" => $login,
					"PASSWORD" => $password,
					"EMAIL" => $email
					);
					
				$res = CEvent::SendImmediate("USER_PASSWORD_RESET_MOD",$siteID,$arFields);

				return true;
			
			}
			else 
			{
				return false;	
			}	
		}
		else 
		{
			return false;
		}
	
	}
	
		
	function onResetpwd($arFields) 
	{
		
		global $APPLICATION;	
	
		// get user ID & login
		$login = $arFields["LOGIN"];
		$rsUser = CUser::GetByLogin($login);
		$arUser = $rsUser->Fetch();
		if($arUser)
		{
			// get user ID
			$uid = $arUser['ID'];
			// get user group ID
			$gid = CUser::GetUserGroup($uid);
			// get default site ID
			$sid = $arUser['LID'];
			// user default site name by site ID
			$siteName = CPwdreset::getSiteName($sid); 
			
			$result = CPwdreset::newPassword($uid,$login,$arUser['EMAIL'],$gid,$sid,$siteName);
			
			// if change password success - return false to prevent send default password reset link
			return !$result;
		} 
		else
		{
			// to show message login not found
			return true;
		}

	}
	
}

?>
