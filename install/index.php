<?

Class pwdreset extends CModule
{
	
	var $MODULE_ID = "pwdreset";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	
	var $EVENT_NAME = "USER_PASSWORD_RESET_MOD";
	
	function EventName()
	{
		return $this->EVENT_NAME;
	}
	
	
	function pwdreset() 
	{
		$arModuleVersion = array();
		
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");
		
		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) 
		{
			$this -> MODULE_VERSION = $arModuleVersion["VERSION"];
			$this -> MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		
		$this -> MODULE_NAME = "Сброс пароля - безопасный сброс пароля";
		$this -> MODULE_DESCRIPTION = "по запросу пользователя сбрасывает и устанавливает новый пароль на основе политики группы";
		
	}
	
	function getSelectedSites()
	{
		return $_REQUEST["LID"];
	}
	
	function RegisterModule()
	{
		
		global $DOCUMENT_ROOT, $APPLICATION, $step;
		
		RegisterModuleDependences("main","OnBeforeUserSendPassword",$this->MODULE_ID,"CPwdreset","onResetpwd");
		
		RegisterModule($this->MODULE_ID);
		
		// create and add mail event
		$arEvent = array(
			"EVENT_NAME" => $this->EVENT_NAME,
			"NAME" => "Сброс и создание нового пользовательского пароля",
			"LID" => "ru",
			"DESCRIPTION" => "
				#SITE_NAME# - название сайта,
				#LOGIN# - логин пользователя,
				#PASSWORD# - новый пароль
				"
			);
		$arEvent["DESCRIPTION"] = preg_replace( '/\h+/', ' ', $arEvent["DESCRIPTION"]); // clear whitespaces
		$obEventType = new CEventType;
		$obEventType->Add($arEvent);
			
		// get list of active sites for enable mail template for these sites
		$sitesId = $this->getSelectedSites();

		// create and add mail template
		$arTemplate = array(
			"ACTIVE" => "Y",
			"EVENT_NAME" => $this->EVENT_NAME,
			"LID" => $sitesId,
			"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL#",
			"BCC" => "",
			"SUBJECT" => "#SITE_NAME#: Сброс пароля",
			"BODY_TYPE" => "text",
			"MESSAGE" => 
				preg_replace( '/\h+/', ' ', "
				Информационное сообщение сайта #SITE_NAME#. Вы запросили сброс пароля
				
				Сайт: #SITE_NAME#
				Login: #LOGIN#
				Новый пароль: #PASSWORD#
				
				Сообщение было сгенерировано автоматически, пожалуйста не отвечайте на него 
				")
		);
		$obTemplate = new CEventMessage;
		$result = $obTemplate->Add($arTemplate);
	
	}
	
	
	function DoInstall()
	{

		global $APPLICATION, $DOCUMENT_ROOT, $GLOBALS, $step;
		
		if((@file_exists($DOCUMENT_ROOT."/bitrix/modules/".$this->MODULE_ID."/install/step1.php"))&&
		   (@file_exists($DOCUMENT_ROOT."/bitrix/modules/".$this->MODULE_ID."/install/step2.php"))&& 
		   (@file_exists($DOCUMENT_ROOT."/bitrix/modules/".$this->MODULE_ID."/install/unstep.php"))&&
		   (@file_exists($DOCUMENT_ROOT."/bitrix/modules/".$this->MODULE_ID."/classes/general/main.php"))) 
		{
			$step = IntVal($step);

			if($step < 2)
			{			
				if(($_REQUEST["install"]=="N")&&($step==1)) 
				{
					return false;
				} 
				else
				{
					$APPLICATION->IncludeAdminFile("Установка модуля \"Сброс пароля\"", $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/pwdreset/install/step1.php");
					return true;
				}
			}
			elseif($step==2)
			{
				if(sizeof($this->getSelectedSites())>0) {
					$this->RegisterModule();
					$GLOBALS["errors"] = $this->errors;
					$APPLICATION->IncludeAdminFile("Установка модуля \"Сброс пароля\"", $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/pwdreset/install/step2.php");
					return true;
				}
				else
				{
					$APPLICATION->IncludeAdminFile("Установка модуля \"Сброс пароля\"", $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/pwdreset/install/step1.php");
					return false;
				}
			}
		}
		else 
		{
			$arNotify = array(
				"MESSAGE"=>"Не найден один или несколько необходимых файлов установки модуля. Пожалуйста, обратитесь к разработчику.",
				"ENABLE_CLOSE"=>"Y"
			);
			CAdminNotify::Add($arNotify);

			return false;
		}
	}
	
	function DoUninstall() 
	{
		global $DOCUMENT_ROOT, $APPLICATION;

		UnRegisterModuleDependences("main","OnBeforeUserChangePassword",$this->MODULE_ID,"CPwdreset","onResetpwd");

		UnRegisterModule($this->MODULE_ID);

		// remove mail template(s)
		$rsMess = CEventMessage::GetList($by="event_name",$order="asc",array("TYPE_ID" => $this->EVENT_NAME));
		$arMess = $rsMess->Fetch(); // get first template if any
		while($arMess) {
			$rsEM = CEventMessage::GetByID($arMess['ID']);
			$arEM = $rsEM->Fetch();
			CEventMessage::Delete($arEM['ID']);
			$arMess = $rsMess->Fetch(); // get next if any
		}
		
		// remove mail event
		$obEventType = new CEventType;
		$obEventType->Delete($this->EVENT_NAME);

		$APPLICATION->IncludeAdminFile("Удаление модуля \"Сброс пароля\"", $DOCUMENT_ROOT."/bitrix/modules/pwdreset/install/unstep.php");
		return true;
	}
	
}

?>
